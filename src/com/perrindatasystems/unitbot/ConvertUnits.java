package com.perrindatasystems.unitbot;

import java.text.DecimalFormat;
import java.util.HashMap;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnKeyListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.LinearLayout.LayoutParams;

import com.perrindatasystems.unitbot.model.Type;

public class ConvertUnits extends AbstractActivity implements TextWatcher {

	private EditText editText;
	private HashMap<Type,TextView> views = new HashMap<Type, TextView>();
	private Type type;
	private DecimalFormat format = new DecimalFormat();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.convert);
		
        syncColor1( this.findViewById(R.id.convert_header));
        syncColor1( this.findViewById(R.id.result_header));
        syncColor2( this.findViewById(R.id.number_input_group));
        syncColor2( this.findViewById(R.id.results_container));        
        		
		type = Type.valueOf(this.getIntent().getStringExtra("type"));
		format.setMaximumFractionDigits(4);
		
		TextView textView = (TextView) findViewById(R.id.convert_header);
		textView.setText("Converting " + type.name + ":");

        LayoutParams adParams = new LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
        adParams.setMargins(0, 0, 1, 0);
        ((ViewGroup)textView.getParent()).addView(this.getAdview(), 0, adParams);
		
		editText = (EditText) findViewById(R.id.number_input);
		editText.addTextChangedListener(this);

		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		if(imm != null){ imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0); }
		
		TableLayout layout = (TableLayout) findViewById(R.id.results_group);
		layout.setStretchAllColumns(true);
		
		TableLayout.LayoutParams params = new TableLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		
		TableRow.LayoutParams rowParams = new TableRow.LayoutParams(160, LayoutParams.WRAP_CONTENT);
		
		TableRow row = null;
		int index = 0;
		
		for(Type otherType : type.grouping.getTypes()) {
			if(otherType == type)
				continue;
		
			if(index % 2 == 0) {
				row = new TableRow(this);
				layout.addView(row, params);
			}
			index++;
			
			TextView text = new TextView(this);
			text.setLayoutParams(rowParams);
			text.setGravity(Gravity.CENTER_HORIZONTAL);
			text.setTextSize(18);
	    	text.setTextColor(Color.argb(255, 0, 0, 0));
			views.put(otherType, text);
			row.addView(text);
			
		}		
		
		updateConversions(1);

	}
	
	@Override
	protected void onResume() {
		super.onResume();
		editText = (EditText) findViewById(R.id.number_input);
		editText.requestFocus();
		editText.setOnKeyListener(new OnKeyListener() {
			
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
			    if (keyCode == KeyEvent.KEYCODE_BACK) {
			    	finish();
			    	return true;
			    }
			    return false;
			}
		});
	}
	
	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
	    	finish();
	    	return true;
	    }
	    return false;
	}
	
	@Override
	public void afterTextChanged(Editable s) {
		
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {
		
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		try {
			double x = Double.parseDouble(editText.getText().toString());
			updateConversions(x);
		} catch( Exception e ) {
			for(TextView view : views.values())
				view.setText("");
		}
	}

	
	private void updateConversions(double x) {
		for(Type otherType : views.keySet()) {
			TextView view = views.get(otherType);
			double y = type.convertTo(otherType, x);
			view.setText(format.format(y) + otherType);			
		}		
	}	
}
