package com.perrindatasystems.unitbot;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

import com.millennialmedia.android.MMAdView;
import com.millennialmedia.android.MMAdView.MMAdListener;

class AbstractActivity extends Activity {

	private static final String MYAPID = "22669";

	private MMAdView adview;
	public ColorTheme theme = ColorTheme.BLUE;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		checkColorTheme();
	}
	
	public MMAdView getAdview() {
		if (adview != null)
			return adview;

		Hashtable<String, String> map = new Hashtable<String, String>();
		map.put("keywords", "unit conversion");

		adview = new MMAdView((Activity) this, MYAPID, "MMBannerAdTop", 30,
				true, map);

		MMAdListener listener = new MyAdListener();
		adview.setListener(listener);
		return adview;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.mainmenu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.menu_about)
			showAbout();
		else if (item.getItemId() == R.id.menu_feedback) {
			doFeedback();
		} else if(item.getItemId() == R.id.menu_theme) {
			showThemePicker();
		} else
			return true;
		return false;
	}

	private void showAbout() {
		ViewGroup aboutView = (ViewGroup) this.getLayoutInflater().inflate(
				R.layout.about, null);
		TextView version = (TextView) aboutView.findViewById(R.id.text_version);
		version.setText("Version " + this.getVersionName(this));

		new AlertDialog.Builder(this).setView(aboutView).setCancelable(true)
				.setPositiveButton("Ok", null).create().show();
	}

	private AlertDialog themePickerDialog = null;
	
	
	private void showThemePicker() {

		
		final ColorTheme[] themes = ColorTheme.values();
		ListView themeList = new ListView(this);
		themeList.setAdapter(new ArrayAdapter<ColorTheme>(this, R.layout.list_item, ColorTheme.values()));

		themeList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int pos,
					long id) {
				
				setColorTheme(themes[pos]);				
				onChangeTheme();
				themePickerDialog.dismiss();
			}
		  });
		
		
		themePickerDialog = new AlertDialog.Builder(this).setView(themeList).setCancelable(true)
			.setNegativeButton("Cancel", null).create();
		
		themePickerDialog.show();		
	}
	
	private HashMap<View, Integer> viewsToSyncColor = new HashMap<View, Integer>();

	protected void syncColor1(View view) {
		viewsToSyncColor.put(view, 1);
		view.setBackgroundColor(theme.color1);
	}
		
	protected void syncColor2(View view) {
		viewsToSyncColor.put(view, 2);
		view.setBackgroundColor(theme.color2);		
	}
		
	private void onChangeTheme() {
		for(Map.Entry<View,Integer> e : viewsToSyncColor.entrySet()) {
			int color = e.getValue() == 1 ? theme.color1 : theme.color2;
			e.getKey().setBackgroundColor(color);
		}
	}
	
	public String getVersionName(Context context) {
		try {
			ComponentName comp = new ComponentName(context, this.getClass());
			PackageInfo pinfo = context.getPackageManager().getPackageInfo(
					comp.getPackageName(), 0);
			return pinfo.versionName;
		} catch (android.content.pm.PackageManager.NameNotFoundException e) {
			return "";
		}
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		checkColorTheme();
		onChangeTheme();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
		case 0:
			if (resultCode == RESULT_CANCELED) {
				Log.i("myApp", "OVERLAY CLOSED MAIN ACTIVITY ");
			}
		}
	}
	
	public void checkColorTheme() {
		 SharedPreferences settings = this.getSharedPreferences("UNITBOT",0);
		 String colorThemeName = settings.getString("colorTheme", "BLUE");
		 theme = ColorTheme.BLUE;
		 try {
			 theme = ColorTheme.valueOf(colorThemeName);
		 } catch( Exception e ) {}
	}
	
	public void setColorTheme(ColorTheme theme) {
		  SharedPreferences settings = this.getSharedPreferences("UNITBOT",0);
	      SharedPreferences.Editor editor = settings.edit();
	      editor.putString("colorTheme", theme.name());
	      editor.commit();
	      this.theme = theme;
	}

	/* Methods to implement as part of the listener interface */
	public class MyAdListener implements MMAdListener {
		public void MMAdFailed(MMAdView adview) {
			Log.i("MyApp", "Millennial Ad View Failed");
		}

		public void MMAdReturned(MMAdView adview) {
			Log.i("MyApp", "Millennial Ad View Success");
		}

		public void MMAdClickedToNewBrowser(MMAdView adview) {
			Log.i("MyApp", "Millennial Ad clicked, new browser launched");
		}

		public void MMAdClickedToOverlay(MMAdView adview) {
			Log.i("MyApp", "Millennial Ad Clicked to overlay");
		}

		public void MMAdOverlayLaunched(MMAdView adview) {
			Log.i("MyApp", "Millennial Ad Overlay Launched");
		}
	}

	@Override
	public void onPause() {
		super.onPause();
		adview.halt();
	}

	private void doFeedback() {
		final Intent emailIntent = new Intent(
				android.content.Intent.ACTION_SEND);
		emailIntent.setType("plain/text");
		emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL,
				new String[] { "apps@perrindatasystems.com" });
		emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,
				"Feedback for UnitBot V" + getVersionName(this));
		startActivity(Intent.createChooser(emailIntent, "Send mail..."));
	}

}
