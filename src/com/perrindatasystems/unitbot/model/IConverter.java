package com.perrindatasystems.unitbot.model;

public interface IConverter {

	public double convertFrom(Type type, double x);
	
	public double convertTo(Type type, double x);
}
