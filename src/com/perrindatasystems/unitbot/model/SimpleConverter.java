package com.perrindatasystems.unitbot.model;

public class SimpleConverter extends AbstractConverter implements IConverter {

	private double factor;

	public SimpleConverter(Type thisType, double factor) {
		super(thisType);
		this.factor = factor;
	}
	
	@Override
	public double convertFrom(Type type, double x) {
		if(type == groupDefault())
			return x  / factor;
		return super.convertFrom(type, x);
	}

	@Override
	public double convertTo(Type type, double x) {
		if(type == groupDefault())
			return x * factor;
		return super.convertTo(type, x);
	}
	
}
 