package com.perrindatasystems.unitbot.model;

public class FarenheitConverter extends AbstractConverter {

	public FarenheitConverter(Type type) {
		super(type);
	}

	@Override
	public double convertFrom(Type type, double x) {
		if(type == Type.CELCIUS)
			return (x*9/5)+32;
		return super.convertFrom(type, x);
	}
	
	@Override
	public double convertTo(Type type, double x) {
		if(type == Type.CELCIUS)
			return (x-32)*5/9;
		return super.convertTo(type, x);
	}
	
}
