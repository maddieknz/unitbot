package com.perrindatasystems.unitbot.model;

public class AbstractConverter implements IConverter {

	private Type thisType;

	public AbstractConverter(Type thisType) {
		this.thisType = thisType;
	}
	
	public Type groupDefault() {
		return thisType.grouping.dflt();
	}
		
	@Override
	public double convertFrom(Type type, double x) {
		if(thisType == type)
			return x;
		else
			throw new UnsupportedOperationException("Cannot convert from " + type + " to " + thisType);
	}
	
	@Override
	public double convertTo(Type type, double x) {
		if(type != groupDefault())
			x = this.convertTo(groupDefault(), x);
		return type.convertFrom(groupDefault(), x);
	}
}
