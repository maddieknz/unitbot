package com.perrindatasystems.unitbot.model;

import java.util.ArrayList;

public enum Grouping {
	Distance("M"),
	Speed("MPH"),
	Temperature("CELCIUS"),
	Baking("CUP"),
	Area("SQ_KM"),
	Fluids("LITERS"),
	Energy("KCAL");
	
	private Type dflt;
	private String dfltName;
	
	Grouping(String dflt) {
		this.dfltName = dflt;
	}
	
	public Type dflt() {
		if(dflt == null)
			dflt = Type.valueOf(dfltName);
		return dflt;
	}

	public Type[] getTypes() {
		ArrayList<Type> list= new ArrayList<Type>(10);
		for(Type type : Type.values())
			if(type.grouping == this)
				list.add(type);
		return list.toArray(new Type[list.size()]);
	}
}
