package com.perrindatasystems.unitbot.model;

public class KelvinConverter extends AbstractConverter {

	public KelvinConverter(Type type) {
		super(type);
	}

	@Override
	public double convertFrom(Type type, double x) {
		if(type == Type.CELCIUS)
			return x + 273.15;
		return super.convertFrom(type, x);
	}
	
	@Override
	public double convertTo(Type type, double x) {
		if(type == Type.CELCIUS)
			return x - 273.15;
		return super.convertTo(type, x);
	}
	
}
