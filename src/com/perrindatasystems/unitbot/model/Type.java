package com.perrindatasystems.unitbot.model;

import java.lang.reflect.Constructor;

public enum Type {
	
	M("m",Grouping.Distance),
	KM("km",Grouping.Distance,1000),
	MILES("miles",Grouping.Distance,1609.344),
	FEET("feet",Grouping.Distance,0.3048),
	YARD("yards",Grouping.Distance,0.9144),
	
	MPH("mph",Grouping.Speed),
	KMH("km/h",Grouping.Speed,0.621371192),
	METER_PER_SEC("m/s",Grouping.Speed,2.23693629),
	KNOTS("knots",Grouping.Speed,1.15077945),
	MACH("mach",Grouping.Speed,761.2),

	CELCIUS("�C",Grouping.Temperature),
	FARENHEIT("�F",Grouping.Temperature,FarenheitConverter.class),
	KELVIN("�K",Grouping.Temperature,KelvinConverter.class),
	
	TEASPOON("teaspoon",Grouping.Baking,0.0208333333),
	TABLESPOON("tablespoon",Grouping.Baking,0.0625),
	CUP("cups",Grouping.Baking),
	
	LITERS("L",Grouping.Fluids),
	FL_OZ("fl.oz.",Grouping.Fluids, 0.0295735296),
	PINT("pints",Grouping.Fluids, 0.473176473),
	QUARTS("quarts",Grouping.Fluids,0.946352946),
	GALLONS("gallons",Grouping.Fluids,3.78541178),	
	ML("mL",Grouping.Fluids,0.001),
	
	SQ_KM("Sq Km",Grouping.Area),
	SQ_MILES("Sq Miles",Grouping.Area,2.58998811),
	SQ_FEET("Sq Feet",Grouping.Area,1/10763910.4),
	ACRES("Acres",Grouping.Area,0.00404685642),
	
	KCAL("KCal",Grouping.Energy),
	KJ("KJ",Grouping.Energy,239.005736),
	KWH("KWH",Grouping.Energy,860.42065);
	
	public String name;
	public Grouping grouping;
	public IConverter converter;
		
	Type(String name, Grouping grouping) {
		this.name = name;
		this.grouping = grouping;
		this.converter = new AbstractConverter(this);
	}
	
	Type(String name, Grouping grouping, double factor) {
		this.name = name;
		this.grouping = grouping;
		this.converter = new SimpleConverter(this,factor);
	}

	Type(String name, Grouping grouping, Class<? extends IConverter> clz) {
		this.name = name;
		this.grouping = grouping;
		try {
			Constructor<? extends IConverter> constructor = clz.getConstructor(Type.class);
			converter = constructor.newInstance(this);
		} catch (Exception e) {
		}		
	}
	
	@Override
	public String toString() {
		return " " + name;
	}
	
	public double convertTo(Type type, double x) {
		return this.converter.convertTo(type, x);
	}

	public double convertFrom(Type type, double x) {
		return this.converter.convertFrom(type, x);
	}	
}
