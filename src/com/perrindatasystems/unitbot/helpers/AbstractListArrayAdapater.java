package com.perrindatasystems.unitbot.helpers;

import android.widget.ListAdapter;

public abstract class AbstractListArrayAdapater extends AbstractArrayAdapter
	implements ListAdapter {

    public AbstractListArrayAdapater(Object[] objects) {
	super(objects);
    }

    @Override
    public boolean areAllItemsEnabled() {
	return true;
    }

    @Override
    public boolean isEnabled(int position) {
	return true;
    }
}
