package com.perrindatasystems.unitbot.helpers;

import java.util.LinkedList;

import android.database.DataSetObserver;
import android.widget.Adapter;

public abstract class AbstractArrayAdapter implements Adapter {

    private Object[] objects;
    private LinkedList<DataSetObserver> observers = new LinkedList<DataSetObserver>();

    public AbstractArrayAdapter(Object[] objects) {
	this.objects = objects;
    }

    public void setObjects(Object[] objects) {
	this.objects = objects;
	for (DataSetObserver observer : observers)
	    observer.onInvalidated();
    }

    public void invalidateData() {
	for (DataSetObserver observer : observers)
	    observer.onInvalidated();
    }

    @Override
    public int getCount() {
	return objects.length;
    }

    @Override
    public Object getItem(int position) {
	return objects[position];
    }

    @Override
    public long getItemId(int position) {
	return position;
    }

    @Override
    public int getItemViewType(int position) {
	return 0;
    }

    @Override
    public int getViewTypeCount() {
	return 1;
    }

    @Override
    public boolean hasStableIds() {
	return true;
    }

    @Override
    public boolean isEmpty() {
	return getCount() == 0;
    }

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {
	observers.add(observer);
    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {
	observers.remove(observer);
    }

}
