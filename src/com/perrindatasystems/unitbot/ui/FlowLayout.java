package com.perrindatasystems.unitbot.ui;

import java.util.Map;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

public class FlowLayout extends ViewGroup
{
	public static class LayoutParams extends ViewGroup.MarginLayoutParams
	{
		public LayoutParams(Context c, AttributeSet attrs)
		{
			super(c, attrs);
		}

		public LayoutParams(int w, int h)
		{
			super(w, h);
		}

	}

	public FlowLayout(Context context)
	{
		super(context);
	}

	public FlowLayout(Context context, AttributeSet attrs, Map inflateParams,
			int defStyle)
	{
		super(context, attrs, defStyle);
	}

	@Override
	public LayoutParams generateLayoutParams(AttributeSet attrs)
	{
		return new FlowLayout.LayoutParams(this.getContext(), attrs);
	}

	protected int getVirtualChildCount()
	{
		return getChildCount();
	}

	protected View getVirtualChildAt(int index)
	{
		return getChildAt(index);
	}

	@Override
	protected void onLayout(boolean changed, int left, int top, int right, int bottom)
	{
		int cnt = getVirtualChildCount();
		int w = right - left - this.getPaddingLeft() - this.getPaddingRight();

		int rowEnds[] = new int[cnt];
		int rowHeights[] = new int[cnt];

		int maxRow = 0;
		int rowWidth = 0;
		
		rowEnds[0] = 0;
		rowHeights[0] = 0;
		
		// Organize in rows
		for (int i = 0; i != cnt; ++i)
		{
			View c = getVirtualChildAt(i);

			if (c == null || c.getVisibility() == GONE)
				continue;
			
			int width = c.getMeasuredWidth();
			int height = c.getMeasuredHeight();
			if(rowWidth + width >= w) {
				rowEnds[maxRow] = i;
				maxRow++;
				rowHeights[maxRow] = 0;
				rowWidth = 0;
			}
			
			rowHeights[maxRow] = Math.max(rowHeights[maxRow],height);
			rowWidth += width;
		}

		rowEnds[maxRow] = cnt;
		
		int y = this.getPaddingTop();
		int i = 0;
		
		// Layout a row at a time
		for( int row = 0; row <= maxRow; row++ ) {
			int x = this.getPaddingLeft();

			for( ; i < rowEnds[row]; i++ ) {
				View c = getVirtualChildAt(i);

				if (c == null || c.getVisibility() == GONE)
					continue;
				
				c.layout(x,y,x+c.getMeasuredWidth(),y+rowHeights[row]);
				x += c.getMeasuredWidth();
			}

			y += rowHeights[row];
		}
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
	{
		int cnt = getVirtualChildCount();

		for (int i = 0; i != cnt; ++i)
		{
			View c = getVirtualChildAt(i);

			if (c == null || c.getVisibility() == GONE)
				continue;

			measureChildWithMargins(c, MeasureSpec.UNSPECIFIED, 0, heightMeasureSpec, 0);
		}

		// calculate total measured height of children, and maximum width

		int maxWidth = 0;
		int rowWidth = 0;
		int rowHeight = 0;
		int totalHeight = 0;

		int hmode = MeasureSpec.getMode(widthMeasureSpec);
		int hdim = MeasureSpec.getSize(widthMeasureSpec) - this.getPaddingLeft() - this.getPaddingRight();
		boolean constrained = hmode == MeasureSpec.AT_MOST || hmode == MeasureSpec.EXACTLY;
		
		for (int i = 0; i != cnt; ++i)
		{
			View c = getVirtualChildAt(i);
			if (c == null || c.getVisibility() == GONE)
				continue;

			int width = c.getMeasuredWidth();
			int height = c.getMeasuredHeight();

			for(;;) {
				if(constrained && rowWidth + width >= hdim ) {
					if(rowWidth == 0) {
						// Bad ass doesn't fit on a row by itself
						int wspec = MeasureSpec.makeMeasureSpec(hdim, MeasureSpec.AT_MOST);
						measureChildWithMargins(c, wspec, 0, heightMeasureSpec, 0);
						width = c.getMeasuredWidth();
						continue;
					}
										
					// Wrap to next row
					totalHeight += rowHeight;
					maxWidth = Math.max(maxWidth, rowWidth);
					rowHeight = 0; rowWidth = 0;
					continue;
				}
				
				rowWidth += width;
				rowHeight = Math.max(rowHeight,height);
				break;
			}
		}
		
		// Calculate last row
		totalHeight += rowHeight;
		maxWidth = Math.max(maxWidth, rowWidth);		

		totalHeight += this.getPaddingTop() + this.getPaddingBottom();
		
		if(constrained)
			maxWidth = hdim;
		
		maxWidth += this.getPaddingLeft() + this.getPaddingRight();
		
		setMeasuredDimension(maxWidth, totalHeight);
	}

	public int getBaseline()
	{
		return -1;
	}

	public void setBaselineAligned(boolean baselineAligned)
	{
	}

	public void setVerticalGravity(int verticalGravity)
	{
	}

	public void setGravity(int gravity)
	{
	}
}
