package com.perrindatasystems.unitbot;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.LinearLayout.LayoutParams;

import com.perrindatasystems.unitbot.model.Grouping;
import com.perrindatasystems.unitbot.model.Type;
import com.perrindatasystems.unitbot.ui.FlowLayout;

public class BrowseTypes extends AbstractActivity implements OnClickListener {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        LinearLayout content = new LinearLayout(this);

        content.setOrientation(LinearLayout.VERTICAL);

        LayoutParams adParams = new LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
        adParams.setMargins(0, 0, 1, 0);
        content.addView(this.getAdview(), adParams);
        
        FlowLayout.LayoutParams buttonParams = new FlowLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);        
        LayoutParams groupParams = new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT, 0);      
        
        LayoutParams headerParams = new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT, 0);
        TextView pageHeader = new TextView(this);
    	pageHeader.setLayoutParams(headerParams);
    	pageHeader.setText("Select unit of measure to convert");
    	pageHeader.setTextSize(20);
    	pageHeader.setTextColor(Color.argb(255, 0, 0, 0));
    	syncColor2(pageHeader);
    	content.addView(pageHeader);
    	
        headerParams = new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT, 0);
        headerParams.setMargins(0, 1, 0, 1);
        
       	for(Grouping grouping : Grouping.values()) {
        	Type[] types = grouping.getTypes();
        	if(types.length == 0)
        		continue;
        	
        	TextView groupHeader = new TextView(this);
        	groupHeader.setLayoutParams(headerParams);
        	groupHeader.setText(grouping.toString());
        	groupHeader.setTextSize(23);
        	groupHeader.setTextColor(Color.argb(255, 0, 0, 0));
        	syncColor1(groupHeader);
        	content.addView(groupHeader);
        	
        	FlowLayout groupLayout = new FlowLayout(this);
        	groupLayout.setPadding(4, 6, 4, 1);
//        	groupLayout.setOrientation(LinearLayout.HORIZONTAL);
        	syncColor2(groupLayout);
        	groupLayout.setLayoutParams(groupParams);
            
            for(Type type : types) {
            	Button button = new Button(this);
            	button.setText(type.name);
            	button.setTag(type);
            	button.setLayoutParams(buttonParams);
            	button.setOnClickListener(this);
            	button.setTextSize(19);
            	
            	groupLayout.addView(button);
            }

            content.addView(groupLayout);
        }
        
        ScrollView scroll = new ScrollView(this);
        scroll.addView(content);
        
        setContentView(scroll);
    }


	@Override
	public void onClick(View v) {
		if(v instanceof Button) {
			Type type = (Type) v.getTag();
			Intent intent = new Intent();
			intent.setClass(this, ConvertUnits.class);
			intent.putExtra("type", type.name());
			startActivity(intent);
		}
	}
}