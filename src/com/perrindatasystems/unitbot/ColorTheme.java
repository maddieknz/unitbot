package com.perrindatasystems.unitbot;

import android.graphics.Color;

public enum ColorTheme {

	BLUE("Blue",Color.argb(255, 102, 170, 255),Color.argb(255, 221, 221, 242)),
	GREEN("Green",Color.argb(255, 102, 255, 170),Color.argb(255, 85, 187, 68)),
	RED("Red",Color.argb(255, 255, 34, 68),Color.argb(255, 221, 153, 160)),
	PURPLE("Purple",Color.argb(255, 204, 68, 255),Color.argb(255, 221, 170, 238));

	public final int color1;
	public final int color2;
	public final String display;
	
	private ColorTheme(String display, int color1, int color2) {
		this.color1 = color1;
		this.color2 = color2;
		this.display = display;
	}
	
	@Override
	public String toString() {
		return display;
	}	
}
